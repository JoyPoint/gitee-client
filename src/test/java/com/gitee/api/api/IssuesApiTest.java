package com.gitee.api.api;

import com.gitee.api.ApiClient;
import com.gitee.api.model.Issue;
import org.junit.Before;
import org.junit.Test;


/**
 * API tests for IssuesApi
 */
public class IssuesApiTest {

    private IssuesApi api;

    @Before
    public void setup() {
        api = new ApiClient().createService(IssuesApi.class);
    }

    /**
     * 删除Issue某条评论
     *
     * 删除Issue某条评论
     */
    @Test
    public void deleteV5ReposOwnerRepoIssuesCommentsIdTest() {
        String owner = null;
        String repo = null;
        Integer id = null;
        String accessToken = null;
        // Void response = api.deleteV5ReposOwnerRepoIssuesCommentsId(owner, repo, id, accessToken);

        // TODO: test validations
    }
    /**
     * 获取当前授权用户的所有Issue
     *
     * 获取当前授权用户的所有Issue
     */
    @Test
    public void getV5IssuesTest() {
        String accessToken = null;
        String filter = null;
        String state = null;
        String labels = null;
        String sort = null;
        String direction = null;
        String since = null;
        Integer page = null;
        Integer perPage = null;
        // java.util.List<Issue> response = api.getV5Issues(accessToken, filter, state, labels, sort, direction, since, page, perPage);

        // TODO: test validations
    }
    /**
     * 获取当前用户某个组织的Issues
     *
     * 获取当前用户某个组织的Issues
     */
    @Test
    public void getV5OrgsOrgIssuesTest() {
        String org = null;
        String accessToken = null;
        String filter = null;
        String state = null;
        String labels = null;
        String sort = null;
        String direction = null;
        String since = null;
        Integer page = null;
        Integer perPage = null;
        // java.util.List<Issue> response = api.getV5OrgsOrgIssues(org, accessToken, filter, state, labels, sort, direction, since, page, perPage);

        // TODO: test validations
    }
    /**
     * 项目的所有Issues
     *
     * 项目的所有Issues
     */
    @Test
    public void getV5ReposOwnerRepoIssuesTest() {
        String owner = null;
        String repo = null;
        String accessToken = null;
        String state = null;
        String labels = null;
        String sort = null;
        String direction = null;
        String since = null;
        Integer page = null;
        Integer perPage = null;
        String milestone = null;
        String assignee = null;
        String creator = null;
        // java.util.List<Issue> response = api.getV5ReposOwnerRepoIssues(owner, repo, accessToken, state, labels, sort, direction, since, page, perPage, milestone, assignee, creator);

        // TODO: test validations
    }
    /**
     * 获取项目所有Issue的评论
     *
     * 获取项目所有Issue的评论
     */
    @Test
    public void getV5ReposOwnerRepoIssuesCommentsTest() {
        String owner = null;
        String repo = null;
        String accessToken = null;
        String sort = null;
        String direction = null;
        String since = null;
        Integer page = null;
        Integer perPage = null;
        // Void response = api.getV5ReposOwnerRepoIssuesComments(owner, repo, accessToken, sort, direction, since, page, perPage);

        // TODO: test validations
    }
    /**
     * 获取项目Issue某条评论
     *
     * 获取项目Issue某条评论
     */
    @Test
    public void getV5ReposOwnerRepoIssuesCommentsIdTest() {
        String owner = null;
        String repo = null;
        Integer id = null;
        String accessToken = null;
        // Void response = api.getV5ReposOwnerRepoIssuesCommentsId(owner, repo, id, accessToken);

        // TODO: test validations
    }
    /**
     * 项目的某个Issue
     *
     * 项目的某个Issue
     */
    @Test
    public void getV5ReposOwnerRepoIssuesNumberTest() {
        String owner = null;
        String repo = null;
        String number = null;
        String accessToken = null;
        // Issue response = api.getV5ReposOwnerRepoIssuesNumber(owner, repo, number, accessToken);

        // TODO: test validations
    }
    /**
     * 获取项目某个Issue所有的评论
     *
     * 获取项目某个Issue所有的评论
     */
    @Test
    public void getV5ReposOwnerRepoIssuesNumberCommentsTest() {
        String owner = null;
        String repo = null;
        String number = null;
        String accessToken = null;
        String since = null;
        Integer page = null;
        Integer perPage = null;
        // Void response = api.getV5ReposOwnerRepoIssuesNumberComments(owner, repo, number, accessToken, since, page, perPage);

        // TODO: test validations
    }
    /**
     * 获取当前授权用户的所有Issues
     *
     * 获取当前授权用户的所有Issues
     */
    @Test
    public void getV5UserIssuesTest() {
        String accessToken = null;
        String filter = null;
        String state = null;
        String labels = null;
        String sort = null;
        String direction = null;
        String since = null;
        Integer page = null;
        Integer perPage = null;
        // java.util.List<Issue> response = api.getV5UserIssues(accessToken, filter, state, labels, sort, direction, since, page, perPage);

        // TODO: test validations
    }
    /**
     * 更新Issue某条评论
     *
     * 更新Issue某条评论
     */
    @Test
    public void patchV5ReposOwnerRepoIssuesCommentsIdTest() {
        String owner = null;
        String repo = null;
        Integer id = null;
        String body = null;
        String accessToken = null;
        // Void response = api.patchV5ReposOwnerRepoIssuesCommentsId(owner, repo, id, body, accessToken);

        // TODO: test validations
    }
    /**
     * 更新Issue
     *
     * 更新Issue
     */
    @Test
    public void patchV5ReposOwnerRepoIssuesNumberTest() {
        String owner = null;
        String repo = null;
        String number = null;
        String title = null;
        String accessToken = null;
        String state = null;
        String body = null;
        String assignee = null;
        Integer milestone = null;
        java.util.List<String> labels = null;
        // Issue response = api.patchV5ReposOwnerRepoIssuesNumber(owner, repo, number, title, accessToken, state, body, assignee, milestone, labels);

        // TODO: test validations
    }
    /**
     * 创建Issue
     *
     * 创建Issue
     */
    @Test
    public void postV5ReposOwnerRepoIssuesTest() {
        String owner = null;
        String repo = null;
        String title = null;
        String accessToken = null;
        String body = null;
        String assignee = null;
        Integer milestone = null;
        java.util.List<String> labels = null;
        // Issue response = api.postV5ReposOwnerRepoIssues(owner, repo, title, accessToken, body, assignee, milestone, labels);

        // TODO: test validations
    }
    /**
     * 创建某个Issue评论
     *
     * 创建某个Issue评论
     */
    @Test
    public void postV5ReposOwnerRepoIssuesNumberCommentsTest() {
        String owner = null;
        String repo = null;
        String number = null;
        String body = null;
        String accessToken = null;
        // Void response = api.postV5ReposOwnerRepoIssuesNumberComments(owner, repo, number, body, accessToken);

        // TODO: test validations
    }
}
