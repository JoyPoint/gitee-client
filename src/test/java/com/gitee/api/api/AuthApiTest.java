package com.gitee.api.api;

import com.gitee.api.ApiClient;
import com.gitee.api.model.OAuth2AccessToken;
import org.junit.Before;
import org.junit.Test;
import rx.Observable;
import rx.Observer;

/**
 * created by wuyu on 2017/11/14
 */
public class AuthApiTest {

    private AuthApi api;

    @Before
    public void setup() {
        api = new ApiClient().createService(AuthApi.class);
    }

    @Test
    public void getTokenTest() {
        Observable<OAuth2AccessToken> token = api.getToken("邮箱",
                "密码",
                "重定向网站",
                "clientId",
                "clientSecret",
                "password",
                "projects user_info issues notes");

        token.subscribe(new Observer<OAuth2AccessToken>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable throwable) {
                System.err.println(throwable.getMessage());
            }

            @Override
            public void onNext(OAuth2AccessToken oAuth2AccessToken) {
                System.err.println(oAuth2AccessToken);
            }
        });
    }
}
