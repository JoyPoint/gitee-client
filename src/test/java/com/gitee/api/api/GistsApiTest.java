package com.gitee.api.api;

import com.gitee.api.ApiClient;
import com.gitee.api.model.Code;
import com.gitee.api.model.CodeComment;
import com.gitee.api.model.CodeForks;
import com.gitee.api.model.CodeForksHistory;
import org.junit.Before;
import org.junit.Test;


/**
 * API tests for GistsApi
 */
public class GistsApiTest {

    private GistsApi api;

    @Before
    public void setup() {
        api = new ApiClient().createService(GistsApi.class);
    }

    /**
     * 删除代码片段的评论
     *
     * 删除代码片段的评论
     */
    @Test
    public void deleteV5GistsGistIdCommentsIdTest() {
        String gistId = null;
        Integer id = null;
        String accessToken = null;
        // Void response = api.deleteV5GistsGistIdCommentsId(gistId, id, accessToken);

        // TODO: test validations
    }
    /**
     * 删除该条代码片段
     *
     * 删除该条代码片段
     */
    @Test
    public void deleteV5GistsIdTest() {
        String id = null;
        String accessToken = null;
        // Void response = api.deleteV5GistsId(id, accessToken);

        // TODO: test validations
    }
    /**
     * 取消Star代码片段
     *
     * 取消Star代码片段
     */
    @Test
    public void deleteV5GistsIdStarTest() {
        String id = null;
        String accessToken = null;
        // Void response = api.deleteV5GistsIdStar(id, accessToken);

        // TODO: test validations
    }
    /**
     * 获取代码片段
     *
     * 获取代码片段
     */
    @Test
    public void getV5GistsTest() {
        String accessToken = null;
        String since = null;
        Integer page = null;
        Integer perPage = null;
        // java.util.List<Code> response = api.getV5Gists(accessToken, since, page, perPage);

        // TODO: test validations
    }
    /**
     * 获取代码片段的评论
     *
     * 获取代码片段的评论
     */
    @Test
    public void getV5GistsGistIdCommentsTest() {
        String gistId = null;
        String accessToken = null;
        Integer page = null;
        Integer perPage = null;
        // java.util.List<CodeComment> response = api.getV5GistsGistIdComments(gistId, accessToken, page, perPage);

        // TODO: test validations
    }
    /**
     * 获取单条代码片段的评论
     *
     * 获取单条代码片段的评论
     */
    @Test
    public void getV5GistsGistIdCommentsIdTest() {
        String gistId = null;
        Integer id = null;
        String accessToken = null;
        // CodeComment response = api.getV5GistsGistIdCommentsId(gistId, id, accessToken);

        // TODO: test validations
    }
    /**
     * 获取单条代码片段
     *
     * 获取单条代码片段
     */
    @Test
    public void getV5GistsIdTest() {
        String id = null;
        String accessToken = null;
        // CodeForksHistory response = api.getV5GistsId(id, accessToken);

        // TODO: test validations
    }
    /**
     * 获取代码片段的commit
     *
     * 获取代码片段的commit
     */
    @Test
    public void getV5GistsIdCommitsTest() {
        String id = null;
        String accessToken = null;
        // CodeForksHistory response = api.getV5GistsIdCommits(id, accessToken);

        // TODO: test validations
    }
    /**
     * 获取Fork该条代码片段的列表
     *
     * 获取Fork该条代码片段的列表
     */
    @Test
    public void getV5GistsIdForksTest() {
        String id = null;
        String accessToken = null;
        Integer page = null;
        Integer perPage = null;
        // CodeForks response = api.getV5GistsIdForks(id, accessToken, page, perPage);

        // TODO: test validations
    }
    /**
     * 判断代码片段是否已Star
     *
     * 判断代码片段是否已Star
     */
    @Test
    public void getV5GistsIdStarTest() {
        String id = null;
        String accessToken = null;
        // Void response = api.getV5GistsIdStar(id, accessToken);

        // TODO: test validations
    }
    /**
     * 获取公开的代码片段
     *
     * 获取公开的代码片段
     */
    @Test
    public void getV5GistsPublicTest() {
        String accessToken = null;
        String since = null;
        Integer page = null;
        Integer perPage = null;
        // java.util.List<Code> response = api.getV5GistsPublic(accessToken, since, page, perPage);

        // TODO: test validations
    }
    /**
     * 获取用户Star的代码片段
     *
     * 获取用户Star的代码片段
     */
    @Test
    public void getV5GistsStarredTest() {
        String accessToken = null;
        String since = null;
        Integer page = null;
        Integer perPage = null;
        // java.util.List<Code> response = api.getV5GistsStarred(accessToken, since, page, perPage);

        // TODO: test validations
    }
    /**
     * 获取指定用户的公开代码片段
     *
     * 获取指定用户的公开代码片段
     */
    @Test
    public void getV5UsersUsernameGistsTest() {
        String username = null;
        String accessToken = null;
        Integer page = null;
        Integer perPage = null;
        // java.util.List<Code> response = api.getV5UsersUsernameGists(username, accessToken, page, perPage);

        // TODO: test validations
    }
    /**
     * 修改代码片段的评论
     *
     * 修改代码片段的评论
     */
    @Test
    public void patchV5GistsGistIdCommentsIdTest() {
        String gistId = null;
        Integer id = null;
        String body = null;
        String accessToken = null;
        // CodeComment response = api.patchV5GistsGistIdCommentsId(gistId, id, body, accessToken);

        // TODO: test validations
    }
    /**
     * 修改代码片段
     *
     * 修改代码片段
     */
    @Test
    public void patchV5GistsIdTest() {
        String id = null;
        java.util.Map<String, String> files = null;
        String description = null;
        String accessToken = null;
        Boolean _public = null;
        // CodeForksHistory response = api.patchV5GistsId(id, files, description, accessToken, _public);

        // TODO: test validations
    }
    /**
     * 创建代码片段
     *
     * 创建代码片段
     */
    @Test
    public void postV5GistsTest() {
        java.util.Map<String, String> files = null;
        String description = null;
        String accessToken = null;
        Boolean _public = null;
        // java.util.List<CodeForksHistory> response = api.postV5Gists(files, description, accessToken, _public);

        // TODO: test validations
    }
    /**
     * 增加代码片段的评论
     *
     * 增加代码片段的评论
     */
    @Test
    public void postV5GistsGistIdCommentsTest() {
        String gistId = null;
        String body = null;
        String accessToken = null;
        // CodeComment response = api.postV5GistsGistIdComments(gistId, body, accessToken);

        // TODO: test validations
    }
    /**
     * Fork代码片段
     *
     * Fork代码片段
     */
    @Test
    public void postV5GistsIdForksTest() {
        String id = null;
        String accessToken = null;
        // Void response = api.postV5GistsIdForks(id, accessToken);

        // TODO: test validations
    }
    /**
     * Star代码片段
     *
     * Star代码片段
     */
    @Test
    public void putV5GistsIdStarTest() {
        String id = null;
        String accessToken = null;
        // Void response = api.putV5GistsIdStar(id, accessToken);

        // TODO: test validations
    }
}
