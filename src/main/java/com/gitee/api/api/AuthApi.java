package com.gitee.api.api;

import com.gitee.api.model.OAuth2AccessToken;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rx.Observable;

/**
 * 码云 Auth 验证
 * see: https://gitee.com/api/v5/oauth_doc
 * created by wuyu on 2017/11/14
 */
public interface AuthApi {

    /**
     * @param code
     * @param clientId
     * @param redirectUri
     * @param clientSecret
     * @param grantType    授权类型 code
     * @return
     */
    @POST(value = "/oauth/token")
    @Headers(value = {"Content-Type:application/x-www-form-urlencode"})
    Observable<OAuth2AccessToken> getToken(@Query("code") String code,
                                           @Query("client_id") String clientId,
                                           @Query("redirect_uri") String redirectUri,
                                           @Query("client_secret") String clientSecret,
                                           @Query("grant_type") String grantType
    );

    /**
     * @param username     用户名
     * @param password     密码
     * @param clientId
     * @param redirectUri
     * @param clientSecret
     * @param grantType    授权类型 password
     * @param scope        权限范围，有以下选项: user_info, projects, pull_requests, issues, notes, keys, hook, groups, gists
     * @return
     */
    @POST(value = "/oauth/token")
    @Headers(value = {"Content-Type:application/x-www-form-urlencode"})
    Observable<OAuth2AccessToken> getToken(@Query("username") String username,
                                           @Query("password") String password,
                                           @Query("redirect_uri") String redirectUri,
                                           @Query("client_id") String clientId,
                                           @Query("client_secret") String clientSecret,
                                           @Query("grant_type") String grantType,
                                           @Query("scope") String scope
    );

    /**
     * 刷新token,码云暂时没有提供
     */
//    Observable<OAuth2AccessToken> refreshToken();

}
