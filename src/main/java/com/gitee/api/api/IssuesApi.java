package com.gitee.api.api;

import com.gitee.api.model.Issue;
import com.gitee.api.model.IssueComment;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import rx.Observable;

import java.util.List;


public interface IssuesApi {
    /**
     * 删除Issue某条评论
     * 删除Issue某条评论
     *
     * @param owner       用户名(username/login) (required)
     * @param repo        项目路径(path) (required)
     * @param id          评论的ID (required)
     * @param accessToken 用户授权码 (optional)
     * @return Call&lt;Void&gt;
     */
    @DELETE("v5/repos/{owner}/{repo}/issues/comments/{id}")
    Observable<Void> deleteV5ReposOwnerRepoIssuesCommentsId(
            @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("id") Integer id, @retrofit2.http.Query("access_token") String accessToken
    );

    /**
     * 获取当前授权用户的所有Issue
     * 获取当前授权用户的所有Issue
     *
     * @param accessToken 用户授权码 (optional)
     * @param filter      筛选参数: 授权用户负责的(assigned)，授权用户创建的(created)，包含前两者的(all)。默认: assigned (optional, default to assigned)
     * @param state       Issue的状态: open, closed, or all。 默认: open (optional, default to open)
     * @param labels      用逗号分开的标签。如: bug,performance (optional)
     * @param sort        排序依据: 创建时间(created)，更新时间(updated_at)。默认: created_at (optional, default to created)
     * @param direction   排序方式: 升序(asc)，降序(desc)。默认: desc (optional, default to desc)
     * @param since       起始的更新时间，要求时间格式为 ISO 8601 (optional)
     * @param page        当前的页码 (optional, default to 1)
     * @param perPage     每页的数量 (optional, default to 20)
     * @return Call&lt;java.util.List&lt;Issue&gt;&gt;
     */
    @GET("v5/issues")
    Observable<java.util.List<Issue>> getV5Issues(
            @retrofit2.http.Query("access_token") String accessToken, @retrofit2.http.Query("filter") String filter, @retrofit2.http.Query("state") String state, @retrofit2.http.Query("labels") String labels, @retrofit2.http.Query("sort") String sort, @retrofit2.http.Query("direction") String direction, @retrofit2.http.Query("since") String since, @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("per_page") Integer perPage
    );

    /**
     * 获取当前用户某个组织的Issues
     * 获取当前用户某个组织的Issues
     *
     * @param org         组织的路径(path/login) (required)
     * @param accessToken 用户授权码 (optional)
     * @param filter      筛选参数: 授权用户负责的(assigned)，授权用户创建的(created)，包含前两者的(all)。默认: assigned (optional, default to assigned)
     * @param state       Issue的状态: open, closed, or all。 默认: open (optional, default to open)
     * @param labels      用逗号分开的标签。如: bug,performance (optional)
     * @param sort        排序依据: 创建时间(created)，更新时间(updated_at)。默认: created_at (optional, default to created)
     * @param direction   排序方式: 升序(asc)，降序(desc)。默认: desc (optional, default to desc)
     * @param since       起始的更新时间，要求时间格式为 ISO 8601 (optional)
     * @param page        当前的页码 (optional, default to 1)
     * @param perPage     每页的数量 (optional, default to 20)
     * @return Call&lt;java.util.List&lt;Issue&gt;&gt;
     */
    @GET("v5/orgs/{org}/issues")
    Observable<java.util.List<Issue>> getV5OrgsOrgIssues(
            @retrofit2.http.Path("org") String org, @retrofit2.http.Query("access_token") String accessToken, @retrofit2.http.Query("filter") String filter, @retrofit2.http.Query("state") String state, @retrofit2.http.Query("labels") String labels, @retrofit2.http.Query("sort") String sort, @retrofit2.http.Query("direction") String direction, @retrofit2.http.Query("since") String since, @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("per_page") Integer perPage
    );

    /**
     * 项目的所有Issues
     * 项目的所有Issues
     *
     * @param owner       用户名(username/login) (required)
     * @param repo        项目路径(path) (required)
     * @param accessToken 用户授权码 (optional)
     * @param state       Issue的状态: open, closed, or all。 默认: open (optional, default to open)
     * @param labels      用逗号分开的标签。如: bug,performance (optional)
     * @param sort        排序依据: 创建时间(created)，更新时间(updated_at)。默认: created_at (optional, default to created)
     * @param direction   排序方式: 升序(asc)，降序(desc)。默认: desc (optional, default to desc)
     * @param since       起始的更新时间，要求时间格式为 ISO 8601 (optional)
     * @param page        当前的页码 (optional, default to 1)
     * @param perPage     每页的数量 (optional, default to 20)
     * @param milestone   根据里程碑标题。none为没里程碑的，*为所有带里程碑的 (optional)
     * @param assignee    用户的username。 none为没指派者, *为所有带有指派者的 (optional)
     * @param creator     创建Issues的用户username (optional)
     * @return Call&lt;java.util.List&lt;Issue&gt;&gt;
     */
    @GET("v5/repos/{owner}/{repo}/issues")
    Observable<java.util.List<Issue>> getV5ReposOwnerRepoIssues(
            @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Query("access_token") String accessToken, @retrofit2.http.Query("state") String state, @retrofit2.http.Query("labels") String labels, @retrofit2.http.Query("sort") String sort, @retrofit2.http.Query("direction") String direction, @retrofit2.http.Query("since") String since, @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("per_page") Integer perPage, @retrofit2.http.Query("milestone") String milestone, @retrofit2.http.Query("assignee") String assignee, @retrofit2.http.Query("creator") String creator
    );

    /**
     * 获取项目所有Issue的评论
     * 获取项目所有Issue的评论
     *
     * @param owner       用户名(username/login) (required)
     * @param repo        项目路径(path) (required)
     * @param accessToken 用户授权码 (optional)
     * @param sort        Either created or updated. Default: created (optional, default to created)
     * @param direction   Either asc or desc. Ignored without the sort parameter. (optional, default to asc)
     * @param since       Only comments updated at or after this time are returned.                                               This is a timestamp in ISO 8601 format: YYYY-MM-DDTHH:MM:SSZ (optional)
     * @param page        当前的页码 (optional, default to 1)
     * @param perPage     每页的数量 (optional, default to 20)
     * @return Call&lt;Void&gt;
     */
    @GET("v5/repos/{owner}/{repo}/issues/comments")
    Observable<List<IssueComment>> getV5ReposOwnerRepoIssuesComments(
            @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Query("access_token") String accessToken, @retrofit2.http.Query("sort") String sort, @retrofit2.http.Query("direction") String direction, @retrofit2.http.Query("since") String since, @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("per_page") Integer perPage
    );

    /**
     * 获取项目Issue某条评论
     * 获取项目Issue某条评论
     *
     * @param owner       用户名(username/login) (required)
     * @param repo        项目路径(path) (required)
     * @param id          评论的ID (required)
     * @param accessToken 用户授权码 (optional)
     * @return Call&lt;Void&gt;
     */
    @GET("v5/repos/{owner}/{repo}/issues/comments/{id}")
    Observable<IssueComment> getV5ReposOwnerRepoIssuesCommentsId(
            @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("id") Integer id, @retrofit2.http.Query("access_token") String accessToken
    );

    /**
     * 项目的某个Issue
     * 项目的某个Issue
     *
     * @param owner       用户名(username/login) (required)
     * @param repo        项目路径(path) (required)
     * @param number      Issue 编号(区分大小写) (required)
     * @param accessToken 用户授权码 (optional)
     * @return Call&lt;Issue&gt;
     */
    @GET("v5/repos/{owner}/{repo}/issues/{number}")
    Observable<Issue> getV5ReposOwnerRepoIssuesNumber(
            @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("number") String number, @retrofit2.http.Query("access_token") String accessToken
    );

    /**
     * 获取项目某个Issue所有的评论
     * 获取项目某个Issue所有的评论
     *
     * @param owner       用户名(username/login) (required)
     * @param repo        项目路径(path) (required)
     * @param number      Issue 编号(区分大小写) (required)
     * @param accessToken 用户授权码 (optional)
     * @param since       Only comments updated at or after this time are returned.                                               This is a timestamp in ISO 8601 format: YYYY-MM-DDTHH:MM:SSZ (optional)
     * @param page        当前的页码 (optional, default to 1)
     * @param perPage     每页的数量 (optional, default to 20)
     * @return Call&lt;Void&gt;
     */
    @GET("v5/repos/{owner}/{repo}/issues/{number}/comments")
    Observable<List<IssueComment>> getV5ReposOwnerRepoIssuesNumberComments(
            @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("number") String number, @retrofit2.http.Query("access_token") String accessToken, @retrofit2.http.Query("since") String since, @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("per_page") Integer perPage
    );

    /**
     * 获取当前授权用户的所有Issues
     * 获取当前授权用户的所有Issues
     *
     * @param accessToken 用户授权码 (optional)
     * @param filter      筛选参数: 授权用户负责的(assigned)，授权用户创建的(created)，包含前两者的(all)。默认: assigned (optional, default to assigned)
     * @param state       Issue的状态: open, closed, or all。 默认: open (optional, default to open)
     * @param labels      用逗号分开的标签。如: bug,performance (optional)
     * @param sort        排序依据: 创建时间(created)，更新时间(updated_at)。默认: created_at (optional, default to created)
     * @param direction   排序方式: 升序(asc)，降序(desc)。默认: desc (optional, default to desc)
     * @param since       起始的更新时间，要求时间格式为 ISO 8601 (optional)
     * @param page        当前的页码 (optional, default to 1)
     * @param perPage     每页的数量 (optional, default to 20)
     * @return Call&lt;java.util.List&lt;Issue&gt;&gt;
     */
    @GET("v5/user/issues")
    Observable<java.util.List<Issue>> getV5UserIssues(
            @retrofit2.http.Query("access_token") String accessToken, @retrofit2.http.Query("filter") String filter, @retrofit2.http.Query("state") String state, @retrofit2.http.Query("labels") String labels, @retrofit2.http.Query("sort") String sort, @retrofit2.http.Query("direction") String direction, @retrofit2.http.Query("since") String since, @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("per_page") Integer perPage
    );

    /**
     * 更新Issue某条评论
     * 更新Issue某条评论
     *
     * @param owner       用户名(username/login) (required)
     * @param repo        项目路径(path) (required)
     * @param id          评论的ID (required)
     * @param body        The contents of the comment. (required)
     * @param accessToken 用户授权码 (optional)
     * @return Call&lt;Void&gt;
     */
    @retrofit2.http.FormUrlEncoded
    @PATCH("v5/repos/{owner}/{repo}/issues/comments/{id}")
    Observable<Void> patchV5ReposOwnerRepoIssuesCommentsId(
            @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("id") Integer id, @retrofit2.http.Field("body") String body, @retrofit2.http.Field("access_token") String accessToken
    );

    /**
     * 更新Issue
     * 更新Issue
     *
     * @param owner       用户名(username/login) (required)
     * @param repo        项目路径(path) (required)
     * @param number      Issue 编号(区分大小写) (required)
     * @param title       Issue标题 (required)
     * @param accessToken 用户授权码 (optional)
     * @param state       Issue 状态，open、started、closed 或 approved (optional)
     * @param body        Issue描述 (optional)
     * @param assignee    Issue负责人的username (optional)
     * @param milestone   所属里程碑的number(第几个) (optional)
     * @param labels      标签。如: [\&quot;bug\&quot;,\&quot;performance\&quot;]。此处试验直接对标签名换行即可，如: bug performance (optional)
     * @return Call&lt;Issue&gt;
     */
    @retrofit2.http.FormUrlEncoded
    @PATCH("v5/repos/{owner}/{repo}/issues/{number}")
    Observable<Issue> patchV5ReposOwnerRepoIssuesNumber(
            @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("number") String number, @retrofit2.http.Field("title") String title, @retrofit2.http.Field("access_token") String accessToken, @retrofit2.http.Field("state") String state, @retrofit2.http.Field("body") String body, @retrofit2.http.Field("assignee") String assignee, @retrofit2.http.Field("milestone") Integer milestone, @retrofit2.http.Field("labels") java.util.List<String> labels
    );

    /**
     * 创建Issue
     * 创建Issue
     *
     * @param owner       用户名(username/login) (required)
     * @param repo        项目路径(path) (required)
     * @param title       Issue标题 (required)
     * @param accessToken 用户授权码 (optional)
     * @param body        Issue描述 (optional)
     * @param assignee    Issue负责人的username (optional)
     * @param milestone   所属里程碑的number(第几个) (optional)
     * @param labels      标签。如: [\&quot;bug\&quot;,\&quot;performance\&quot;]。此处试验直接对标签名换行即可，如: bug performance (optional)
     * @return Call&lt;Issue&gt;
     */
    @retrofit2.http.FormUrlEncoded
    @POST("v5/repos/{owner}/{repo}/issues")
    Observable<Issue> postV5ReposOwnerRepoIssues(
            @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Field("title") String title, @retrofit2.http.Field("access_token") String accessToken, @retrofit2.http.Field("body") String body, @retrofit2.http.Field("assignee") String assignee, @retrofit2.http.Field("milestone") Integer milestone, @retrofit2.http.Field("labels") java.util.List<String> labels
    );

    /**
     * 创建某个Issue评论
     * 创建某个Issue评论
     *
     * @param owner       用户名(username/login) (required)
     * @param repo        项目路径(path) (required)
     * @param number      Issue 编号(区分大小写) (required)
     * @param body        The contents of the comment. (required)
     * @param accessToken 用户授权码 (optional)
     * @return Call&lt;Void&gt;
     */
    @retrofit2.http.FormUrlEncoded
    @POST("v5/repos/{owner}/{repo}/issues/{number}/comments")
    Observable<Void> postV5ReposOwnerRepoIssuesNumberComments(
            @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("number") String number, @retrofit2.http.Field("body") String body, @retrofit2.http.Field("access_token") String accessToken
    );

}
