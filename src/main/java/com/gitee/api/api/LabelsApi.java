package com.gitee.api.api;

import com.gitee.api.model.Label;
import retrofit2.http.*;
import rx.Observable;


public interface LabelsApi {
  /**
   * 删除Issue所有标签
   * 删除Issue所有标签
   * @param owner 用户名(username/login) (required)
   * @param repo 项目路径(path) (required)
   * @param number Issue 编号(区分大小写) (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;Void&gt;
   */
  @DELETE("v5/repos/{owner}/{repo}/issues/{number}/labels")
  Observable<Void> deleteV5ReposOwnerRepoIssuesNumberLabels(
    @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("number") String number, @retrofit2.http.Query("access_token") String accessToken
  );

  /**
   * 删除Issue标签
   * 删除Issue标签
   * @param owner 用户名(username/login) (required)
   * @param repo 项目路径(path) (required)
   * @param number Issue 编号(区分大小写) (required)
   * @param name 标签名称 (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;Void&gt;
   */
  @DELETE("v5/repos/{owner}/{repo}/issues/{number}/labels/{name}")
  Observable<Void> deleteV5ReposOwnerRepoIssuesNumberLabelsName(
    @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("number") String number, @retrofit2.http.Path("name") String name, @retrofit2.http.Query("access_token") String accessToken
  );

  /**
   * 删除一个项目标签
   * 删除一个项目标签
   * @param owner 用户名(username/login) (required)
   * @param repo 项目路径(path) (required)
   * @param name 标签名称 (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;Void&gt;
   */
  @DELETE("v5/repos/{owner}/{repo}/labels/{name}")
  Observable<Void> deleteV5ReposOwnerRepoLabelsName(
    @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("name") String name, @retrofit2.http.Query("access_token") String accessToken
  );

  /**
   * 获取项目Issue的所有标签
   * 获取项目Issue的所有标签
   * @param owner 用户名(username/login) (required)
   * @param repo 项目路径(path) (required)
   * @param number Issue 编号(区分大小写) (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;java.util.List&lt;Label&gt;&gt;
   */
  @GET("v5/repos/{owner}/{repo}/issues/{number}/labels")
  Observable<java.util.List<Label>> getV5ReposOwnerRepoIssuesNumberLabels(
    @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("number") String number, @retrofit2.http.Query("access_token") String accessToken
  );

  /**
   * 获取项目所有标签
   * 获取项目所有标签
   * @param owner 用户名(username/login) (required)
   * @param repo 项目路径(path) (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;java.util.List&lt;Label&gt;&gt;
   */
  @GET("v5/repos/{owner}/{repo}/labels")
  Observable<java.util.List<Label>> getV5ReposOwnerRepoLabels(
    @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Query("access_token") String accessToken
  );

  /**
   * 根据标签名称获取单个标签
   * 根据标签名称获取单个标签
   * @param owner 用户名(username/login) (required)
   * @param repo 项目路径(path) (required)
   * @param name 标签名称 (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;Label&gt;
   */
  @GET("v5/repos/{owner}/{repo}/labels/{name}")
  Observable<Label> getV5ReposOwnerRepoLabelsName(
    @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("name") String name, @retrofit2.http.Query("access_token") String accessToken
  );

  /**
   * 更新一个项目标签
   * 更新一个项目标签
   * @param owner 用户名(username/login) (required)
   * @param repo 项目路径(path) (required)
   * @param originalName  (required)
   * @param accessToken 用户授权码 (optional)
   * @param name The name of a label (optional)
   * @param color The color of a label (optional)
   * @return Call&lt;Label&gt;
   */
  @retrofit2.http.FormUrlEncoded
  @PATCH("v5/repos/{owner}/{repo}/labels/{original_name}")
  Observable<Label> patchV5ReposOwnerRepoLabelsOriginalName(
    @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("original_name") Integer originalName, @retrofit2.http.Field("access_token") String accessToken, @retrofit2.http.Field("name") String name, @retrofit2.http.Field("color") String color
  );

  /**
   * 创建Issue标签
   * 创建Issue标签  需要在请求的body里填上数组，元素为标签的名字。如: [\&quot;performance\&quot;, \&quot;bug\&quot;]
   * @param owner 用户名(username/login) (required)
   * @param repo 项目路径(path) (required)
   * @param number Issue 编号(区分大小写) (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;Label&gt;
   */
  @retrofit2.http.FormUrlEncoded
  @POST("v5/repos/{owner}/{repo}/issues/{number}/labels")
  Observable<Label> postV5ReposOwnerRepoIssuesNumberLabels(
    @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("number") String number, @retrofit2.http.Field("access_token") String accessToken
  );

  /**
   * 创建项目标签
   * 创建项目标签
   * @param owner 用户名(username/login) (required)
   * @param repo 项目路径(path) (required)
   * @param name 标签名称 (required)
   * @param color 标签颜色。为6位的数字，如: 000000 (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;Label&gt;
   */
  @retrofit2.http.FormUrlEncoded
  @POST("v5/repos/{owner}/{repo}/labels")
  Observable<Label> postV5ReposOwnerRepoLabels(
    @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Field("name") String name, @retrofit2.http.Field("color") String color, @retrofit2.http.Field("access_token") String accessToken
  );

  /**
   * 替换Issue所有标签
   * 替换Issue所有标签  需要在请求的body里填上数组，元素为标签的名字。如: [\&quot;performance\&quot;, \&quot;bug\&quot;]
   * @param owner 用户名(username/login) (required)
   * @param repo 项目路径(path) (required)
   * @param number Issue 编号(区分大小写) (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;Label&gt;
   */
  @retrofit2.http.FormUrlEncoded
  @PUT("v5/repos/{owner}/{repo}/issues/{number}/labels")
  Observable<Label> putV5ReposOwnerRepoIssuesNumberLabels(
    @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("number") String number, @retrofit2.http.Field("access_token") String accessToken
  );

}
