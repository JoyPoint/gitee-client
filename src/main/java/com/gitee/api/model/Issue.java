package com.gitee.api.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

/**
 * this is description
 */
@ApiModel(description = "this is description")

@javax.annotation.Generated(value = "io.swagger.generator.online.JavaClientCodegenImpl", date = "2017-11-16T13:14:09.140+08:00")

public class Issue   {
  @SerializedName("id")
  private Integer id = null;

  @SerializedName("url")
  private String url = null;

  @SerializedName("repository_url")
  private String repositoryUrl = null;

  @SerializedName("labels_url")
  private String labelsUrl = null;

  @SerializedName("events_url")
  private String eventsUrl = null;

  @SerializedName("html_url")
  private String htmlUrl = null;

  @SerializedName("number")
  private String number = null;

  @SerializedName("state")
  private String state = null;

  @SerializedName("title")
  private String title = null;

  @SerializedName("body")
  private String body = null;

  @SerializedName("user")
  private IssueUser user = null;

  @SerializedName("labels")
  
  private java.util.List<Object> labels = null;

  @SerializedName("assignee")
  private Object assignee = null;

  @SerializedName("repository")
  private IssueRepository repository = null;

  @SerializedName("created_at")
  private java.util.Date createdAt = null;

  @SerializedName("updated_at")
  private java.util.Date updatedAt = null;

  @SerializedName("comments")
  private Integer comments = null;

  public Issue id(Integer id) {
    this.id = id;
    return this;
  }

   /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(example = "612750", value = "")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Issue url(String url) {
    this.url = url;
    return this;
  }

   /**
   * Get url
   * @return url
  **/
  @ApiModelProperty(example = "https://gitee.com/api/v5/repos/wuyu15255872976/spring-boot-starter-dubbo/issues/ID4SU", value = "")
  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public Issue repositoryUrl(String repositoryUrl) {
    this.repositoryUrl = repositoryUrl;
    return this;
  }

   /**
   * Get repositoryUrl
   * @return repositoryUrl
  **/
  @ApiModelProperty(example = "https://gitee.com/api/v5/repos/wuyu15255872976/spring-boot-starter-dubbo", value = "")
  public String getRepositoryUrl() {
    return repositoryUrl;
  }

  public void setRepositoryUrl(String repositoryUrl) {
    this.repositoryUrl = repositoryUrl;
  }

  public Issue labelsUrl(String labelsUrl) {
    this.labelsUrl = labelsUrl;
    return this;
  }

   /**
   * Get labelsUrl
   * @return labelsUrl
  **/
  @ApiModelProperty(example = "https://gitee.com/api/v5/repos/wuyu15255872976/spring-boot-starter-dubbo/issues/ID4SU/labels", value = "")
  public String getLabelsUrl() {
    return labelsUrl;
  }

  public void setLabelsUrl(String labelsUrl) {
    this.labelsUrl = labelsUrl;
  }

  public Issue eventsUrl(String eventsUrl) {
    this.eventsUrl = eventsUrl;
    return this;
  }

   /**
   * Get eventsUrl
   * @return eventsUrl
  **/
  @ApiModelProperty(example = "https://gitee.com/api/v5/repos/wuyu15255872976/spring-boot-starter-dubbo/issues/ID4SU/events", value = "")
  public String getEventsUrl() {
    return eventsUrl;
  }

  public void setEventsUrl(String eventsUrl) {
    this.eventsUrl = eventsUrl;
  }

  public Issue htmlUrl(String htmlUrl) {
    this.htmlUrl = htmlUrl;
    return this;
  }

   /**
   * Get htmlUrl
   * @return htmlUrl
  **/
  @ApiModelProperty(example = "https://gitee.com/wuyu15255872976/spring-boot-starter-dubbo/issues/ID4SU", value = "")
  public String getHtmlUrl() {
    return htmlUrl;
  }

  public void setHtmlUrl(String htmlUrl) {
    this.htmlUrl = htmlUrl;
  }

  public Issue number(String number) {
    this.number = number;
    return this;
  }

   /**
   * Get number
   * @return number
  **/
  @ApiModelProperty(example = "ID4SU", value = "")
  public String getNumber() {
    return number;
  }

  public void setNumber(String number) {
    this.number = number;
  }

  public Issue state(String state) {
    this.state = state;
    return this;
  }

   /**
   * Get state
   * @return state
  **/
  @ApiModelProperty(example = "open", value = "")
  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public Issue title(String title) {
    this.title = title;
    return this;
  }

   /**
   * Get title
   * @return title
  **/
  @ApiModelProperty(example = "我看demo中的例子都是简单的一个对象，dubbo是支持各种复杂的接口的（多对象，各种参数），而feign仅仅支持单个对象，请问这个项目支出不支持多对象", value = "")
  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public Issue body(String body) {
    this.body = body;
    return this;
  }

   /**
   * Get body
   * @return body
  **/
  @ApiModelProperty(example = "", value = "")
  public String getBody() {
    return body;
  }

  public void setBody(String body) {
    this.body = body;
  }

  public Issue user(IssueUser user) {
    this.user = user;
    return this;
  }

   /**
   * Get user
   * @return user
  **/
  @ApiModelProperty(value = "")
  public IssueUser getUser() {
    return user;
  }

  public void setUser(IssueUser user) {
    this.user = user;
  }

  public Issue labels(java.util.List<Object> labels) {
    this.labels = labels;
    return this;
  }

  public Issue addLabelsItem(Object labelsItem) {
    if (this.labels == null) {
      this.labels = new java.util.ArrayList<Object>();
    }
    this.labels.add(labelsItem);
    return this;
  }

   /**
   * Get labels
   * @return labels
  **/
  @ApiModelProperty(value = "")
  public java.util.List<Object> getLabels() {
    return labels;
  }

  public void setLabels(java.util.List<Object> labels) {
    this.labels = labels;
  }

  public Issue assignee(Object assignee) {
    this.assignee = assignee;
    return this;
  }

   /**
   * Get assignee
   * @return assignee
  **/
  @ApiModelProperty(value = "")
  public Object getAssignee() {
    return assignee;
  }

  public void setAssignee(Object assignee) {
    this.assignee = assignee;
  }

  public Issue repository(IssueRepository repository) {
    this.repository = repository;
    return this;
  }

   /**
   * Get repository
   * @return repository
  **/
  @ApiModelProperty(value = "")
  public IssueRepository getRepository() {
    return repository;
  }

  public void setRepository(IssueRepository repository) {
    this.repository = repository;
  }

  public Issue createdAt(java.util.Date createdAt) {
    this.createdAt = createdAt;
    return this;
  }

   /**
   * Get createdAt
   * @return createdAt
  **/
  @ApiModelProperty(example = "2017-05-13T04:51:05+08:00", value = "")
  public java.util.Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(java.util.Date createdAt) {
    this.createdAt = createdAt;
  }

  public Issue updatedAt(java.util.Date updatedAt) {
    this.updatedAt = updatedAt;
    return this;
  }

   /**
   * Get updatedAt
   * @return updatedAt
  **/
  @ApiModelProperty(example = "2017-05-25T09:45:13+08:00", value = "")
  public java.util.Date getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(java.util.Date updatedAt) {
    this.updatedAt = updatedAt;
  }

  public Issue comments(Integer comments) {
    this.comments = comments;
    return this;
  }

   /**
   * Get comments
   * @return comments
  **/
  @ApiModelProperty(example = "1", value = "")
  public Integer getComments() {
    return comments;
  }

  public void setComments(Integer comments) {
    this.comments = comments;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Issue issue = (Issue) o;
    return Objects.equals(this.id, issue.id) &&
        Objects.equals(this.url, issue.url) &&
        Objects.equals(this.repositoryUrl, issue.repositoryUrl) &&
        Objects.equals(this.labelsUrl, issue.labelsUrl) &&
        Objects.equals(this.eventsUrl, issue.eventsUrl) &&
        Objects.equals(this.htmlUrl, issue.htmlUrl) &&
        Objects.equals(this.number, issue.number) &&
        Objects.equals(this.state, issue.state) &&
        Objects.equals(this.title, issue.title) &&
        Objects.equals(this.body, issue.body) &&
        Objects.equals(this.user, issue.user) &&
        Objects.equals(this.labels, issue.labels) &&
        Objects.equals(this.assignee, issue.assignee) &&
        Objects.equals(this.repository, issue.repository) &&
        Objects.equals(this.createdAt, issue.createdAt) &&
        Objects.equals(this.updatedAt, issue.updatedAt) &&
        Objects.equals(this.comments, issue.comments);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, url, repositoryUrl, labelsUrl, eventsUrl, htmlUrl, number, state, title, body, user, labels, assignee, repository, createdAt, updatedAt, comments);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Issue {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    url: ").append(toIndentedString(url)).append("\n");
    sb.append("    repositoryUrl: ").append(toIndentedString(repositoryUrl)).append("\n");
    sb.append("    labelsUrl: ").append(toIndentedString(labelsUrl)).append("\n");
    sb.append("    eventsUrl: ").append(toIndentedString(eventsUrl)).append("\n");
    sb.append("    htmlUrl: ").append(toIndentedString(htmlUrl)).append("\n");
    sb.append("    number: ").append(toIndentedString(number)).append("\n");
    sb.append("    state: ").append(toIndentedString(state)).append("\n");
    sb.append("    title: ").append(toIndentedString(title)).append("\n");
    sb.append("    body: ").append(toIndentedString(body)).append("\n");
    sb.append("    user: ").append(toIndentedString(user)).append("\n");
    sb.append("    labels: ").append(toIndentedString(labels)).append("\n");
    sb.append("    assignee: ").append(toIndentedString(assignee)).append("\n");
    sb.append("    repository: ").append(toIndentedString(repository)).append("\n");
    sb.append("    createdAt: ").append(toIndentedString(createdAt)).append("\n");
    sb.append("    updatedAt: ").append(toIndentedString(updatedAt)).append("\n");
    sb.append("    comments: ").append(toIndentedString(comments)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

