package com.gitee.api.model;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

/**
 * this is description
 */
public class Event   {
  @SerializedName("id")
  private Integer id = null;

  @SerializedName("type")
  private String type = null;

  @SerializedName("actor")
  private EventActor actor = null;

  @SerializedName("repo")
  private EventRepo repo = null;

  @SerializedName("public")
  private Boolean _public = null;

  @SerializedName("created_at")
  private java.util.Date createdAt = null;

  @SerializedName("payload")
  private EventPayload payload = null;

  public Event id(Integer id) {
    this.id = id;
    return this;
  }

   /**
   * Get id
   * @return id
  **/
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Event type(String type) {
    this.type = type;
    return this;
  }

   /**
   * Get type
   * @return type
  **/
  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Event actor(EventActor actor) {
    this.actor = actor;
    return this;
  }

   /**
   * Get actor
   * @return actor
  **/
  public EventActor getActor() {
    return actor;
  }

  public void setActor(EventActor actor) {
    this.actor = actor;
  }

  public Event repo(EventRepo repo) {
    this.repo = repo;
    return this;
  }

   /**
   * Get repo
   * @return repo
  **/
  public EventRepo getRepo() {
    return repo;
  }

  public void setRepo(EventRepo repo) {
    this.repo = repo;
  }

  public Event _public(Boolean _public) {
    this._public = _public;
    return this;
  }

   /**
   * Get _public
   * @return _public
  **/
  public Boolean isPublic() {
    return _public;
  }

  public void setPublic(Boolean _public) {
    this._public = _public;
  }

  public Event createdAt(java.util.Date createdAt) {
    this.createdAt = createdAt;
    return this;
  }

   /**
   * Get createdAt
   * @return createdAt
  **/
  public java.util.Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(java.util.Date createdAt) {
    this.createdAt = createdAt;
  }

  public Event payload(EventPayload payload) {
    this.payload = payload;
    return this;
  }

   /**
   * Get payload
   * @return payload
  **/
  public EventPayload getPayload() {
    return payload;
  }

  public void setPayload(EventPayload payload) {
    this.payload = payload;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Event event = (Event) o;
    return Objects.equals(this.id, event.id) &&
        Objects.equals(this.type, event.type) &&
        Objects.equals(this.actor, event.actor) &&
        Objects.equals(this.repo, event.repo) &&
        Objects.equals(this._public, event._public) &&
        Objects.equals(this.createdAt, event.createdAt) &&
        Objects.equals(this.payload, event.payload);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, type, actor, repo, _public, createdAt, payload);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Event {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    actor: ").append(toIndentedString(actor)).append("\n");
    sb.append("    repo: ").append(toIndentedString(repo)).append("\n");
    sb.append("    _public: ").append(toIndentedString(_public)).append("\n");
    sb.append("    createdAt: ").append(toIndentedString(createdAt)).append("\n");
    sb.append("    payload: ").append(toIndentedString(payload)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

