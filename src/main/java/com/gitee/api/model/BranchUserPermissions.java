package com.gitee.api.model;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

/**
 * BranchUserPermissions
 */
public class BranchUserPermissions {
    @SerializedName("pull")
    private Boolean pull = null;

    @SerializedName("push")
    private Boolean push = null;

    @SerializedName("admin")
    private Boolean admin = null;

    public BranchUserPermissions pull(Boolean pull) {
        this.pull = pull;
        return this;
    }

    /**
     * Get pull
     *
     * @return pull
     **/
    public Boolean isPull() {
        return pull;
    }

    public void setPull(Boolean pull) {
        this.pull = pull;
    }

    public BranchUserPermissions push(Boolean push) {
        this.push = push;
        return this;
    }

    /**
     * Get push
     *
     * @return push
     **/
    public Boolean isPush() {
        return push;
    }

    public void setPush(Boolean push) {
        this.push = push;
    }

    public BranchUserPermissions admin(Boolean admin) {
        this.admin = admin;
        return this;
    }

    /**
     * Get admin
     *
     * @return admin
     **/
    public Boolean isAdmin() {
        return admin;
    }

    public void setAdmin(Boolean admin) {
        this.admin = admin;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        BranchUserPermissions branchUserPermissions = (BranchUserPermissions) o;
        return Objects.equals(this.pull, branchUserPermissions.pull) &&
                Objects.equals(this.push, branchUserPermissions.push) &&
                Objects.equals(this.admin, branchUserPermissions.admin);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pull, push, admin);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class BranchUserPermissions {\n");

        sb.append("    pull: ").append(toIndentedString(pull)).append("\n");
        sb.append("    push: ").append(toIndentedString(push)).append("\n");
        sb.append("    admin: ").append(toIndentedString(admin)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}

