package com.gitee.api.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

/**
 * RepositoriesForkUserOwner
 */
public class RepositoriesForkUserOwner   {
  @SerializedName("login")
  private String login = null;

  @SerializedName("id")
  private Integer id = null;

  @SerializedName("avatar_url")
  private String avatarUrl = null;

  @SerializedName("url")
  private String url = null;

  @SerializedName("html_url")
  private String htmlUrl = null;

  @SerializedName("followers_url")
  private String followersUrl = null;

  @SerializedName("following_url")
  private String followingUrl = null;

  @SerializedName("gists_url")
  private String gistsUrl = null;

  @SerializedName("starred_url")
  private String starredUrl = null;

  @SerializedName("subscriptions_url")
  private String subscriptionsUrl = null;

  @SerializedName("organizations_url")
  private String organizationsUrl = null;

  @SerializedName("repos_url")
  private String reposUrl = null;

  @SerializedName("events_url")
  private String eventsUrl = null;

  @SerializedName("received_events_url")
  private String receivedEventsUrl = null;

  @SerializedName("type")
  private String type = null;

  @SerializedName("site_admin")
  private Boolean siteAdmin = null;

  public RepositoriesForkUserOwner login(String login) {
    this.login = login;
    return this;
  }

   /**
   * Get login
   * @return login
  **/
  @ApiModelProperty(example = "fnet", value = "")
  public String getLogin() {
    return login;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  public RepositoriesForkUserOwner id(Integer id) {
    this.id = id;
    return this;
  }

   /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(example = "139498", value = "")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public RepositoriesForkUserOwner avatarUrl(String avatarUrl) {
    this.avatarUrl = avatarUrl;
    return this;
  }

   /**
   * Get avatarUrl
   * @return avatarUrl
  **/
  public String getAvatarUrl() {
    return avatarUrl;
  }

  public void setAvatarUrl(String avatarUrl) {
    this.avatarUrl = avatarUrl;
  }

  public RepositoriesForkUserOwner url(String url) {
    this.url = url;
    return this;
  }

   /**
   * Get url
   * @return url
  **/
  @ApiModelProperty(example = "https://gitee.com/api/v5/users/fnet", value = "")
  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public RepositoriesForkUserOwner htmlUrl(String htmlUrl) {
    this.htmlUrl = htmlUrl;
    return this;
  }

   /**
   * Get htmlUrl
   * @return htmlUrl
  **/
  @ApiModelProperty(example = "https://gitee.com/fnet", value = "")
  public String getHtmlUrl() {
    return htmlUrl;
  }

  public void setHtmlUrl(String htmlUrl) {
    this.htmlUrl = htmlUrl;
  }

  public RepositoriesForkUserOwner followersUrl(String followersUrl) {
    this.followersUrl = followersUrl;
    return this;
  }

   /**
   * Get followersUrl
   * @return followersUrl
  **/
  @ApiModelProperty(example = "https://gitee.com/api/v5/users/fnet/followers", value = "")
  public String getFollowersUrl() {
    return followersUrl;
  }

  public void setFollowersUrl(String followersUrl) {
    this.followersUrl = followersUrl;
  }

  public RepositoriesForkUserOwner followingUrl(String followingUrl) {
    this.followingUrl = followingUrl;
    return this;
  }

   /**
   * Get followingUrl
   * @return followingUrl
  **/
  @ApiModelProperty(example = "https://gitee.com/api/v5/users/fnet/following_url{/other_user}", value = "")
  public String getFollowingUrl() {
    return followingUrl;
  }

  public void setFollowingUrl(String followingUrl) {
    this.followingUrl = followingUrl;
  }

  public RepositoriesForkUserOwner gistsUrl(String gistsUrl) {
    this.gistsUrl = gistsUrl;
    return this;
  }

   /**
   * Get gistsUrl
   * @return gistsUrl
  **/
  @ApiModelProperty(example = "https://gitee.com/api/v5/users/fnet/gists{/gist_id}", value = "")
  public String getGistsUrl() {
    return gistsUrl;
  }

  public void setGistsUrl(String gistsUrl) {
    this.gistsUrl = gistsUrl;
  }

  public RepositoriesForkUserOwner starredUrl(String starredUrl) {
    this.starredUrl = starredUrl;
    return this;
  }

   /**
   * Get starredUrl
   * @return starredUrl
  **/
  @ApiModelProperty(example = "https://gitee.com/api/v5/users/fnet/starred{/owner}{/repo}", value = "")
  public String getStarredUrl() {
    return starredUrl;
  }

  public void setStarredUrl(String starredUrl) {
    this.starredUrl = starredUrl;
  }

  public RepositoriesForkUserOwner subscriptionsUrl(String subscriptionsUrl) {
    this.subscriptionsUrl = subscriptionsUrl;
    return this;
  }

   /**
   * Get subscriptionsUrl
   * @return subscriptionsUrl
  **/
  @ApiModelProperty(example = "https://gitee.com/api/v5/users/fnet/subscriptions", value = "")
  public String getSubscriptionsUrl() {
    return subscriptionsUrl;
  }

  public void setSubscriptionsUrl(String subscriptionsUrl) {
    this.subscriptionsUrl = subscriptionsUrl;
  }

  public RepositoriesForkUserOwner organizationsUrl(String organizationsUrl) {
    this.organizationsUrl = organizationsUrl;
    return this;
  }

   /**
   * Get organizationsUrl
   * @return organizationsUrl
  **/
  @ApiModelProperty(example = "https://gitee.com/api/v5/users/fnet/orgs", value = "")
  public String getOrganizationsUrl() {
    return organizationsUrl;
  }

  public void setOrganizationsUrl(String organizationsUrl) {
    this.organizationsUrl = organizationsUrl;
  }

  public RepositoriesForkUserOwner reposUrl(String reposUrl) {
    this.reposUrl = reposUrl;
    return this;
  }

   /**
   * Get reposUrl
   * @return reposUrl
  **/
  @ApiModelProperty(example = "https://gitee.com/api/v5/users/fnet/repos", value = "")
  public String getReposUrl() {
    return reposUrl;
  }

  public void setReposUrl(String reposUrl) {
    this.reposUrl = reposUrl;
  }

  public RepositoriesForkUserOwner eventsUrl(String eventsUrl) {
    this.eventsUrl = eventsUrl;
    return this;
  }

   /**
   * Get eventsUrl
   * @return eventsUrl
  **/
  @ApiModelProperty(example = "https://gitee.com/api/v5/users/fnet/events{/privacy}", value = "")
  public String getEventsUrl() {
    return eventsUrl;
  }

  public void setEventsUrl(String eventsUrl) {
    this.eventsUrl = eventsUrl;
  }

  public RepositoriesForkUserOwner receivedEventsUrl(String receivedEventsUrl) {
    this.receivedEventsUrl = receivedEventsUrl;
    return this;
  }

   /**
   * Get receivedEventsUrl
   * @return receivedEventsUrl
  **/
  @ApiModelProperty(example = "https://gitee.com/api/v5/users/fnet/received_events", value = "")
  public String getReceivedEventsUrl() {
    return receivedEventsUrl;
  }

  public void setReceivedEventsUrl(String receivedEventsUrl) {
    this.receivedEventsUrl = receivedEventsUrl;
  }

  public RepositoriesForkUserOwner type(String type) {
    this.type = type;
    return this;
  }

   /**
   * Get type
   * @return type
  **/
  @ApiModelProperty(example = "User", value = "")
  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public RepositoriesForkUserOwner siteAdmin(Boolean siteAdmin) {
    this.siteAdmin = siteAdmin;
    return this;
  }

   /**
   * Get siteAdmin
   * @return siteAdmin
  **/
  @ApiModelProperty(value = "")
  public Boolean isSiteAdmin() {
    return siteAdmin;
  }

  public void setSiteAdmin(Boolean siteAdmin) {
    this.siteAdmin = siteAdmin;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    RepositoriesForkUserOwner repositoriesForkUserOwner = (RepositoriesForkUserOwner) o;
    return Objects.equals(this.login, repositoriesForkUserOwner.login) &&
        Objects.equals(this.id, repositoriesForkUserOwner.id) &&
        Objects.equals(this.avatarUrl, repositoriesForkUserOwner.avatarUrl) &&
        Objects.equals(this.url, repositoriesForkUserOwner.url) &&
        Objects.equals(this.htmlUrl, repositoriesForkUserOwner.htmlUrl) &&
        Objects.equals(this.followersUrl, repositoriesForkUserOwner.followersUrl) &&
        Objects.equals(this.followingUrl, repositoriesForkUserOwner.followingUrl) &&
        Objects.equals(this.gistsUrl, repositoriesForkUserOwner.gistsUrl) &&
        Objects.equals(this.starredUrl, repositoriesForkUserOwner.starredUrl) &&
        Objects.equals(this.subscriptionsUrl, repositoriesForkUserOwner.subscriptionsUrl) &&
        Objects.equals(this.organizationsUrl, repositoriesForkUserOwner.organizationsUrl) &&
        Objects.equals(this.reposUrl, repositoriesForkUserOwner.reposUrl) &&
        Objects.equals(this.eventsUrl, repositoriesForkUserOwner.eventsUrl) &&
        Objects.equals(this.receivedEventsUrl, repositoriesForkUserOwner.receivedEventsUrl) &&
        Objects.equals(this.type, repositoriesForkUserOwner.type) &&
        Objects.equals(this.siteAdmin, repositoriesForkUserOwner.siteAdmin);
  }

  @Override
  public int hashCode() {
    return Objects.hash(login, id, avatarUrl, url, htmlUrl, followersUrl, followingUrl, gistsUrl, starredUrl, subscriptionsUrl, organizationsUrl, reposUrl, eventsUrl, receivedEventsUrl, type, siteAdmin);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class RepositoriesForkUserOwner {\n");
    
    sb.append("    login: ").append(toIndentedString(login)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    avatarUrl: ").append(toIndentedString(avatarUrl)).append("\n");
    sb.append("    url: ").append(toIndentedString(url)).append("\n");
    sb.append("    htmlUrl: ").append(toIndentedString(htmlUrl)).append("\n");
    sb.append("    followersUrl: ").append(toIndentedString(followersUrl)).append("\n");
    sb.append("    followingUrl: ").append(toIndentedString(followingUrl)).append("\n");
    sb.append("    gistsUrl: ").append(toIndentedString(gistsUrl)).append("\n");
    sb.append("    starredUrl: ").append(toIndentedString(starredUrl)).append("\n");
    sb.append("    subscriptionsUrl: ").append(toIndentedString(subscriptionsUrl)).append("\n");
    sb.append("    organizationsUrl: ").append(toIndentedString(organizationsUrl)).append("\n");
    sb.append("    reposUrl: ").append(toIndentedString(reposUrl)).append("\n");
    sb.append("    eventsUrl: ").append(toIndentedString(eventsUrl)).append("\n");
    sb.append("    receivedEventsUrl: ").append(toIndentedString(receivedEventsUrl)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    siteAdmin: ").append(toIndentedString(siteAdmin)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

