
# PullRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  |  [optional]
**url** | **String** |  |  [optional]
**htmlUrl** | **String** |  |  [optional]
**diffUrl** | **String** |  |  [optional]
**patchUrl** | **String** |  |  [optional]
**issueUrl** | **String** |  |  [optional]
**commitsUrl** | **String** |  |  [optional]
**reviewCommentsUrl** | **String** |  |  [optional]
**reviewCommentUrl** | **String** |  |  [optional]
**commentsUrl** | **String** |  |  [optional]
**statusesUrl** | **String** |  |  [optional]
**number** | **String** |  |  [optional]
**state** | **String** |  |  [optional]
**title** | **String** |  |  [optional]
**body** | **String** |  |  [optional]
**assignee** | **String** |  |  [optional]
**milestone** | **String** |  |  [optional]
**locked** | **String** |  |  [optional]
**createdAt** | **String** |  |  [optional]
**updatedAt** | **String** |  |  [optional]
**closedAt** | **String** |  |  [optional]
**mergedAt** | **String** |  |  [optional]
**head** | **String** |  |  [optional]
**base** | **String** |  |  [optional]
**links** | **String** |  |  [optional]
**user** | **String** |  |  [optional]



