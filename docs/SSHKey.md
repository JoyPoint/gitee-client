
# SSHKey

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  |  [optional]
**key** | **String** |  |  [optional]
**url** | **String** |  |  [optional]
**title** | **String** |  |  [optional]
**createdAt** | **String** |  |  [optional]



