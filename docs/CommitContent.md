
# CommitContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**content** | [**ContentBasic**](ContentBasic.md) |  |  [optional]
**commit** | [**Commit**](Commit.md) |  |  [optional]



