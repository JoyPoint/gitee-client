# ActivityApi

All URIs are relative to *https://gitee.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteV5UserStarredOwnerRepo**](ActivityApi.md#deleteV5UserStarredOwnerRepo) | **DELETE** v5/user/starred/{owner}/{repo} | 取消 star 一个项目
[**deleteV5UserSubscriptionsOwnerRepo**](ActivityApi.md#deleteV5UserSubscriptionsOwnerRepo) | **DELETE** v5/user/subscriptions/{owner}/{repo} | 取消 watch 一个项目
[**getV5Events**](ActivityApi.md#getV5Events) | **GET** v5/events | 获取站内所有公开动态
[**getV5NetworksOwnerRepoEvents**](ActivityApi.md#getV5NetworksOwnerRepoEvents) | **GET** v5/networks/{owner}/{repo}/events | 列出项目的所有公开动态
[**getV5NotificationsMessages**](ActivityApi.md#getV5NotificationsMessages) | **GET** v5/notifications/messages | 列出授权用户的所有私信
[**getV5NotificationsMessagesId**](ActivityApi.md#getV5NotificationsMessagesId) | **GET** v5/notifications/messages/{id} | 获取一个私信
[**getV5NotificationsThreads**](ActivityApi.md#getV5NotificationsThreads) | **GET** v5/notifications/threads | 列出授权用户的所有通知
[**getV5NotificationsThreadsId**](ActivityApi.md#getV5NotificationsThreadsId) | **GET** v5/notifications/threads/{id} | 获取一个通知
[**getV5OrgsOrgEvents**](ActivityApi.md#getV5OrgsOrgEvents) | **GET** v5/orgs/{org}/events | 列出组织的公开动态
[**getV5ReposOwnerRepoEvents**](ActivityApi.md#getV5ReposOwnerRepoEvents) | **GET** v5/repos/{owner}/{repo}/events | 列出项目的所有动态
[**getV5ReposOwnerRepoNotifications**](ActivityApi.md#getV5ReposOwnerRepoNotifications) | **GET** v5/repos/{owner}/{repo}/notifications | 列出一个项目里的通知
[**getV5ReposOwnerRepoStargazers**](ActivityApi.md#getV5ReposOwnerRepoStargazers) | **GET** v5/repos/{owner}/{repo}/stargazers | 列出 star 了项目的用户
[**getV5ReposOwnerRepoSubscribers**](ActivityApi.md#getV5ReposOwnerRepoSubscribers) | **GET** v5/repos/{owner}/{repo}/subscribers | 列出 watch 了项目的用户
[**getV5UserStarred**](ActivityApi.md#getV5UserStarred) | **GET** v5/user/starred | 列出授权用户 star 了的项目
[**getV5UserStarredOwnerRepo**](ActivityApi.md#getV5UserStarredOwnerRepo) | **GET** v5/user/starred/{owner}/{repo} | 检查授权用户是否 star 了一个项目
[**getV5UserSubscriptions**](ActivityApi.md#getV5UserSubscriptions) | **GET** v5/user/subscriptions | 列出授权用户 watch 了的项目
[**getV5UserSubscriptionsOwnerRepo**](ActivityApi.md#getV5UserSubscriptionsOwnerRepo) | **GET** v5/user/subscriptions/{owner}/{repo} | 检查授权用户是否 watch 了一个项目
[**getV5UsersUsernameEvents**](ActivityApi.md#getV5UsersUsernameEvents) | **GET** v5/users/{username}/events | 列出用户的动态
[**getV5UsersUsernameEventsOrgsOrg**](ActivityApi.md#getV5UsersUsernameEventsOrgsOrg) | **GET** v5/users/{username}/events/orgs/{org} | 列出用户所属组织的动态
[**getV5UsersUsernameEventsPublic**](ActivityApi.md#getV5UsersUsernameEventsPublic) | **GET** v5/users/{username}/events/public | 列出用户的公开动态
[**getV5UsersUsernameReceivedEvents**](ActivityApi.md#getV5UsersUsernameReceivedEvents) | **GET** v5/users/{username}/received_events | 列出一个用户收到的动态
[**getV5UsersUsernameReceivedEventsPublic**](ActivityApi.md#getV5UsersUsernameReceivedEventsPublic) | **GET** v5/users/{username}/received_events/public | 列出一个用户收到的公开动态
[**getV5UsersUsernameStarred**](ActivityApi.md#getV5UsersUsernameStarred) | **GET** v5/users/{username}/starred | 列出用户 star 了的项目
[**getV5UsersUsernameSubscriptions**](ActivityApi.md#getV5UsersUsernameSubscriptions) | **GET** v5/users/{username}/subscriptions | 列出用户 watch 了的项目
[**patchV5NotificationsMessagesId**](ActivityApi.md#patchV5NotificationsMessagesId) | **PATCH** v5/notifications/messages/{id} | 标记一个私信为已读
[**patchV5NotificationsThreadsId**](ActivityApi.md#patchV5NotificationsThreadsId) | **PATCH** v5/notifications/threads/{id} | 标记一个通知为已读
[**postV5NotificationsMessages**](ActivityApi.md#postV5NotificationsMessages) | **POST** v5/notifications/messages | 发送私信给指定用户
[**putV5NotificationsMessages**](ActivityApi.md#putV5NotificationsMessages) | **PUT** v5/notifications/messages | 标记所有私信为已读
[**putV5NotificationsThreads**](ActivityApi.md#putV5NotificationsThreads) | **PUT** v5/notifications/threads | 标记所有通知为已读
[**putV5ReposOwnerRepoNotifications**](ActivityApi.md#putV5ReposOwnerRepoNotifications) | **PUT** v5/repos/{owner}/{repo}/notifications | 标记一个项目里的通知为已读
[**putV5UserStarredOwnerRepo**](ActivityApi.md#putV5UserStarredOwnerRepo) | **PUT** v5/user/starred/{owner}/{repo} | star 一个项目
[**putV5UserSubscriptionsOwnerRepo**](ActivityApi.md#putV5UserSubscriptionsOwnerRepo) | **PUT** v5/user/subscriptions/{owner}/{repo} | watch 一个项目


<a name="deleteV5UserStarredOwnerRepo"></a>
# **deleteV5UserStarredOwnerRepo**
> Void deleteV5UserStarredOwnerRepo(owner, repo, accessToken)

取消 star 一个项目

取消 star 一个项目

### Example
```java
// Import classes:
//import com.gitee.api.api.ActivityApi;

ActivityApi apiInstance =  new ApiClient().create(ActivityApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.deleteV5UserStarredOwnerRepo(owner, repo, accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="deleteV5UserSubscriptionsOwnerRepo"></a>
# **deleteV5UserSubscriptionsOwnerRepo**
> Void deleteV5UserSubscriptionsOwnerRepo(owner, repo, accessToken)

取消 watch 一个项目

取消 watch 一个项目

### Example
```java
// Import classes:
//import com.gitee.api.api.ActivityApi;

ActivityApi apiInstance =  new ApiClient().create(ActivityApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.deleteV5UserSubscriptionsOwnerRepo(owner, repo, accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5Events"></a>
# **getV5Events**
> java.util.List&lt;Event&gt; getV5Events(accessToken, page, perPage)

获取站内所有公开动态

获取站内所有公开动态

### Example
```java
// Import classes:
//import com.gitee.api.api.ActivityApi;

ActivityApi apiInstance =  new ApiClient().create(ActivityApi.class);
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量
Observable<java.util.List<Event>> result = apiInstance.getV5Events(accessToken, page, perPage);
result.subscribe(new Observer<java.util.List<Event>>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(java.util.List<Event> response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量 | [optional] [default to 20]

### Return type

[**java.util.List&lt;Event&gt;**](Event.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5NetworksOwnerRepoEvents"></a>
# **getV5NetworksOwnerRepoEvents**
> java.util.List&lt;Event&gt; getV5NetworksOwnerRepoEvents(owner, repo, accessToken, page, perPage)

列出项目的所有公开动态

列出项目的所有公开动态

### Example
```java
// Import classes:
//import com.gitee.api.api.ActivityApi;

ActivityApi apiInstance =  new ApiClient().create(ActivityApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量
Observable<java.util.List<Event>> result = apiInstance.getV5NetworksOwnerRepoEvents(owner, repo, accessToken, page, perPage);
result.subscribe(new Observer<java.util.List<Event>>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(java.util.List<Event> response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量 | [optional] [default to 20]

### Return type

[**java.util.List&lt;Event&gt;**](Event.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5NotificationsMessages"></a>
# **getV5NotificationsMessages**
> java.util.List&lt;UserMessage&gt; getV5NotificationsMessages(accessToken, unread, since, before, page, perPage)

列出授权用户的所有私信

列出授权用户的所有私信

### Example
```java
// Import classes:
//import com.gitee.api.api.ActivityApi;

ActivityApi apiInstance =  new ApiClient().create(ActivityApi.class);
String accessToken = "accessToken_example"; // String | 用户授权码
Boolean unread = true; // Boolean | 是否只显示未读私信，默认：否
String since = "since_example"; // String | 只显示在给定时间后更新的私信，要求时间格式为 ISO 8601
String before = "before_example"; // String | 只显示在给定时间前更新的私信，要求时间格式为 ISO 8601
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量
Observable<java.util.List<UserMessage>> result = apiInstance.getV5NotificationsMessages(accessToken, unread, since, before, page, perPage);
result.subscribe(new Observer<java.util.List<UserMessage>>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(java.util.List<UserMessage> response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **String**| 用户授权码 | [optional]
 **unread** | **Boolean**| 是否只显示未读私信，默认：否 | [optional]
 **since** | **String**| 只显示在给定时间后更新的私信，要求时间格式为 ISO 8601 | [optional]
 **before** | **String**| 只显示在给定时间前更新的私信，要求时间格式为 ISO 8601 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量 | [optional] [default to 20]

### Return type

[**java.util.List&lt;UserMessage&gt;**](UserMessage.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5NotificationsMessagesId"></a>
# **getV5NotificationsMessagesId**
> UserMessage getV5NotificationsMessagesId(id, accessToken)

获取一个私信

获取一个私信

### Example
```java
// Import classes:
//import com.gitee.api.api.ActivityApi;

ActivityApi apiInstance =  new ApiClient().create(ActivityApi.class);
Integer id = 56; // Integer | 私信的 ID
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<UserMessage> result = apiInstance.getV5NotificationsMessagesId(id, accessToken);
result.subscribe(new Observer<UserMessage>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(UserMessage response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| 私信的 ID |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**UserMessage**](UserMessage.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5NotificationsThreads"></a>
# **getV5NotificationsThreads**
> java.util.List&lt;UserNotification&gt; getV5NotificationsThreads(accessToken, unread, participating, since, before, page, perPage)

列出授权用户的所有通知

列出授权用户的所有通知

### Example
```java
// Import classes:
//import com.gitee.api.api.ActivityApi;

ActivityApi apiInstance =  new ApiClient().create(ActivityApi.class);
String accessToken = "accessToken_example"; // String | 用户授权码
Boolean unread = true; // Boolean | 是否只显示未读消息，默认：否
Boolean participating = true; // Boolean | 是否只显示自己直接参与的消息，默认：否
String since = "since_example"; // String | 只显示在给定时间后更新的消息，要求时间格式为 ISO 8601
String before = "before_example"; // String | 只显示在给定时间前更新的消息，要求时间格式为 ISO 8601
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量
Observable<java.util.List<UserNotification>> result = apiInstance.getV5NotificationsThreads(accessToken, unread, participating, since, before, page, perPage);
result.subscribe(new Observer<java.util.List<UserNotification>>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(java.util.List<UserNotification> response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **String**| 用户授权码 | [optional]
 **unread** | **Boolean**| 是否只显示未读消息，默认：否 | [optional]
 **participating** | **Boolean**| 是否只显示自己直接参与的消息，默认：否 | [optional]
 **since** | **String**| 只显示在给定时间后更新的消息，要求时间格式为 ISO 8601 | [optional]
 **before** | **String**| 只显示在给定时间前更新的消息，要求时间格式为 ISO 8601 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量 | [optional] [default to 20]

### Return type

[**java.util.List&lt;UserNotification&gt;**](UserNotification.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5NotificationsThreadsId"></a>
# **getV5NotificationsThreadsId**
> UserNotification getV5NotificationsThreadsId(id, accessToken)

获取一个通知

获取一个通知

### Example
```java
// Import classes:
//import com.gitee.api.api.ActivityApi;

ActivityApi apiInstance =  new ApiClient().create(ActivityApi.class);
Integer id = 56; // Integer | 通知的 ID
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<UserNotification> result = apiInstance.getV5NotificationsThreadsId(id, accessToken);
result.subscribe(new Observer<UserNotification>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(UserNotification response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| 通知的 ID |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**UserNotification**](UserNotification.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5OrgsOrgEvents"></a>
# **getV5OrgsOrgEvents**
> java.util.List&lt;Event&gt; getV5OrgsOrgEvents(org, accessToken, page, perPage)

列出组织的公开动态

列出组织的公开动态

### Example
```java
// Import classes:
//import com.gitee.api.api.ActivityApi;

ActivityApi apiInstance =  new ApiClient().create(ActivityApi.class);
String org = "org_example"; // String | 组织的路径(path/login)
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量
Observable<java.util.List<Event>> result = apiInstance.getV5OrgsOrgEvents(org, accessToken, page, perPage);
result.subscribe(new Observer<java.util.List<Event>>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(java.util.List<Event> response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org** | **String**| 组织的路径(path/login) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量 | [optional] [default to 20]

### Return type

[**java.util.List&lt;Event&gt;**](Event.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoEvents"></a>
# **getV5ReposOwnerRepoEvents**
> java.util.List&lt;Event&gt; getV5ReposOwnerRepoEvents(owner, repo, accessToken, page, perPage)

列出项目的所有动态

列出项目的所有动态

### Example
```java
// Import classes:
//import com.gitee.api.api.ActivityApi;

ActivityApi apiInstance =  new ApiClient().create(ActivityApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量
Observable<java.util.List<Event>> result = apiInstance.getV5ReposOwnerRepoEvents(owner, repo, accessToken, page, perPage);
result.subscribe(new Observer<java.util.List<Event>>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(java.util.List<Event> response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量 | [optional] [default to 20]

### Return type

[**java.util.List&lt;Event&gt;**](Event.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoNotifications"></a>
# **getV5ReposOwnerRepoNotifications**
> java.util.List&lt;UserNotification&gt; getV5ReposOwnerRepoNotifications(owner, repo, accessToken, unread, participating, since, before, page, perPage)

列出一个项目里的通知

列出一个项目里的通知

### Example
```java
// Import classes:
//import com.gitee.api.api.ActivityApi;

ActivityApi apiInstance =  new ApiClient().create(ActivityApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
Boolean unread = true; // Boolean | 是否只显示未读消息，默认：否
Boolean participating = true; // Boolean | 是否只显示自己直接参与的消息，默认：否
String since = "since_example"; // String | 只显示在给定时间后更新的消息，要求时间格式为 ISO 8601
String before = "before_example"; // String | 只显示在给定时间前更新的消息，要求时间格式为 ISO 8601
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量
Observable<java.util.List<UserNotification>> result = apiInstance.getV5ReposOwnerRepoNotifications(owner, repo, accessToken, unread, participating, since, before, page, perPage);
result.subscribe(new Observer<java.util.List<UserNotification>>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(java.util.List<UserNotification> response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **unread** | **Boolean**| 是否只显示未读消息，默认：否 | [optional]
 **participating** | **Boolean**| 是否只显示自己直接参与的消息，默认：否 | [optional]
 **since** | **String**| 只显示在给定时间后更新的消息，要求时间格式为 ISO 8601 | [optional]
 **before** | **String**| 只显示在给定时间前更新的消息，要求时间格式为 ISO 8601 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量 | [optional] [default to 20]

### Return type

[**java.util.List&lt;UserNotification&gt;**](UserNotification.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoStargazers"></a>
# **getV5ReposOwnerRepoStargazers**
> java.util.List&lt;UserBasic&gt; getV5ReposOwnerRepoStargazers(owner, repo, accessToken, page, perPage)

列出 star 了项目的用户

列出 star 了项目的用户

### Example
```java
// Import classes:
//import com.gitee.api.api.ActivityApi;

ActivityApi apiInstance =  new ApiClient().create(ActivityApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量
Observable<java.util.List<UserBasic>> result = apiInstance.getV5ReposOwnerRepoStargazers(owner, repo, accessToken, page, perPage);
result.subscribe(new Observer<java.util.List<UserBasic>>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(java.util.List<UserBasic> response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量 | [optional] [default to 20]

### Return type

[**java.util.List&lt;UserBasic&gt;**](UserBasic.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoSubscribers"></a>
# **getV5ReposOwnerRepoSubscribers**
> java.util.List&lt;UserBasic&gt; getV5ReposOwnerRepoSubscribers(owner, repo, accessToken, page, perPage)

列出 watch 了项目的用户

列出 watch 了项目的用户

### Example
```java
// Import classes:
//import com.gitee.api.api.ActivityApi;

ActivityApi apiInstance =  new ApiClient().create(ActivityApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量
Observable<java.util.List<UserBasic>> result = apiInstance.getV5ReposOwnerRepoSubscribers(owner, repo, accessToken, page, perPage);
result.subscribe(new Observer<java.util.List<UserBasic>>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(java.util.List<UserBasic> response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量 | [optional] [default to 20]

### Return type

[**java.util.List&lt;UserBasic&gt;**](UserBasic.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5UserStarred"></a>
# **getV5UserStarred**
> java.util.List&lt;Project&gt; getV5UserStarred(accessToken, sort, direction, page, perPage)

列出授权用户 star 了的项目

列出授权用户 star 了的项目

### Example
```java
// Import classes:
//import com.gitee.api.api.ActivityApi;

ActivityApi apiInstance =  new ApiClient().create(ActivityApi.class);
String accessToken = "accessToken_example"; // String | 用户授权码
String sort = "created"; // String | 根据项目创建时间(created)或最后推送时间(updated)进行排序，默认：创建时间
String direction = "desc"; // String | 按递增(asc)或递减(desc)排序，默认：递减
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量
Observable<java.util.List<Project>> result = apiInstance.getV5UserStarred(accessToken, sort, direction, page, perPage);
result.subscribe(new Observer<java.util.List<Project>>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(java.util.List<Project> response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **String**| 用户授权码 | [optional]
 **sort** | **String**| 根据项目创建时间(created)或最后推送时间(updated)进行排序，默认：创建时间 | [optional] [default to created] [enum: created, last_push]
 **direction** | **String**| 按递增(asc)或递减(desc)排序，默认：递减 | [optional] [default to desc] [enum: asc, desc]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量 | [optional] [default to 20]

### Return type

[**java.util.List&lt;Project&gt;**](Project.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5UserStarredOwnerRepo"></a>
# **getV5UserStarredOwnerRepo**
> Void getV5UserStarredOwnerRepo(owner, repo, accessToken)

检查授权用户是否 star 了一个项目

检查授权用户是否 star 了一个项目

### Example
```java
// Import classes:
//import com.gitee.api.api.ActivityApi;

ActivityApi apiInstance =  new ApiClient().create(ActivityApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.getV5UserStarredOwnerRepo(owner, repo, accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5UserSubscriptions"></a>
# **getV5UserSubscriptions**
> java.util.List&lt;Project&gt; getV5UserSubscriptions(accessToken, sort, direction, page, perPage)

列出授权用户 watch 了的项目

列出授权用户 watch 了的项目

### Example
```java
// Import classes:
//import com.gitee.api.api.ActivityApi;

ActivityApi apiInstance =  new ApiClient().create(ActivityApi.class);
String accessToken = "accessToken_example"; // String | 用户授权码
String sort = "created"; // String | 根据项目创建时间(created)或最后推送时间(updated)进行排序，默认：创建时间
String direction = "desc"; // String | 按递增(asc)或递减(desc)排序，默认：递减
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量
Observable<java.util.List<Project>> result = apiInstance.getV5UserSubscriptions(accessToken, sort, direction, page, perPage);
result.subscribe(new Observer<java.util.List<Project>>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(java.util.List<Project> response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **String**| 用户授权码 | [optional]
 **sort** | **String**| 根据项目创建时间(created)或最后推送时间(updated)进行排序，默认：创建时间 | [optional] [default to created] [enum: created, last_push]
 **direction** | **String**| 按递增(asc)或递减(desc)排序，默认：递减 | [optional] [default to desc] [enum: asc, desc]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量 | [optional] [default to 20]

### Return type

[**java.util.List&lt;Project&gt;**](Project.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5UserSubscriptionsOwnerRepo"></a>
# **getV5UserSubscriptionsOwnerRepo**
> Void getV5UserSubscriptionsOwnerRepo(owner, repo, accessToken)

检查授权用户是否 watch 了一个项目

检查授权用户是否 watch 了一个项目

### Example
```java
// Import classes:
//import com.gitee.api.api.ActivityApi;

ActivityApi apiInstance =  new ApiClient().create(ActivityApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.getV5UserSubscriptionsOwnerRepo(owner, repo, accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5UsersUsernameEvents"></a>
# **getV5UsersUsernameEvents**
> java.util.List&lt;Event&gt; getV5UsersUsernameEvents(username, accessToken, page, perPage)

列出用户的动态

列出用户的动态

### Example
```java
// Import classes:
//import com.gitee.api.api.ActivityApi;

ActivityApi apiInstance =  new ApiClient().create(ActivityApi.class);
String username = "username_example"; // String | 用户名(username/login)
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量
Observable<java.util.List<Event>> result = apiInstance.getV5UsersUsernameEvents(username, accessToken, page, perPage);
result.subscribe(new Observer<java.util.List<Event>>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(java.util.List<Event> response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**| 用户名(username/login) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量 | [optional] [default to 20]

### Return type

[**java.util.List&lt;Event&gt;**](Event.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5UsersUsernameEventsOrgsOrg"></a>
# **getV5UsersUsernameEventsOrgsOrg**
> java.util.List&lt;Event&gt; getV5UsersUsernameEventsOrgsOrg(username, org, accessToken, page, perPage)

列出用户所属组织的动态

列出用户所属组织的动态

### Example
```java
// Import classes:
//import com.gitee.api.api.ActivityApi;

ActivityApi apiInstance =  new ApiClient().create(ActivityApi.class);
String username = "username_example"; // String | 用户名(username/login)
String org = "org_example"; // String | 组织的路径(path/login)
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量
Observable<java.util.List<Event>> result = apiInstance.getV5UsersUsernameEventsOrgsOrg(username, org, accessToken, page, perPage);
result.subscribe(new Observer<java.util.List<Event>>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(java.util.List<Event> response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**| 用户名(username/login) |
 **org** | **String**| 组织的路径(path/login) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量 | [optional] [default to 20]

### Return type

[**java.util.List&lt;Event&gt;**](Event.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5UsersUsernameEventsPublic"></a>
# **getV5UsersUsernameEventsPublic**
> java.util.List&lt;Event&gt; getV5UsersUsernameEventsPublic(username, accessToken, page, perPage)

列出用户的公开动态

列出用户的公开动态

### Example
```java
// Import classes:
//import com.gitee.api.api.ActivityApi;

ActivityApi apiInstance =  new ApiClient().create(ActivityApi.class);
String username = "username_example"; // String | 用户名(username/login)
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量
Observable<java.util.List<Event>> result = apiInstance.getV5UsersUsernameEventsPublic(username, accessToken, page, perPage);
result.subscribe(new Observer<java.util.List<Event>>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(java.util.List<Event> response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**| 用户名(username/login) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量 | [optional] [default to 20]

### Return type

[**java.util.List&lt;Event&gt;**](Event.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5UsersUsernameReceivedEvents"></a>
# **getV5UsersUsernameReceivedEvents**
> java.util.List&lt;Event&gt; getV5UsersUsernameReceivedEvents(username, accessToken, page, perPage)

列出一个用户收到的动态

列出一个用户收到的动态

### Example
```java
// Import classes:
//import com.gitee.api.api.ActivityApi;

ActivityApi apiInstance =  new ApiClient().create(ActivityApi.class);
String username = "username_example"; // String | 用户名(username/login)
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量
Observable<java.util.List<Event>> result = apiInstance.getV5UsersUsernameReceivedEvents(username, accessToken, page, perPage);
result.subscribe(new Observer<java.util.List<Event>>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(java.util.List<Event> response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**| 用户名(username/login) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量 | [optional] [default to 20]

### Return type

[**java.util.List&lt;Event&gt;**](Event.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5UsersUsernameReceivedEventsPublic"></a>
# **getV5UsersUsernameReceivedEventsPublic**
> java.util.List&lt;Event&gt; getV5UsersUsernameReceivedEventsPublic(username, accessToken, page, perPage)

列出一个用户收到的公开动态

列出一个用户收到的公开动态

### Example
```java
// Import classes:
//import com.gitee.api.api.ActivityApi;

ActivityApi apiInstance =  new ApiClient().create(ActivityApi.class);
String username = "username_example"; // String | 用户名(username/login)
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量
Observable<java.util.List<Event>> result = apiInstance.getV5UsersUsernameReceivedEventsPublic(username, accessToken, page, perPage);
result.subscribe(new Observer<java.util.List<Event>>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(java.util.List<Event> response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**| 用户名(username/login) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量 | [optional] [default to 20]

### Return type

[**java.util.List&lt;Event&gt;**](Event.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5UsersUsernameStarred"></a>
# **getV5UsersUsernameStarred**
> java.util.List&lt;Project&gt; getV5UsersUsernameStarred(username, accessToken, page, perPage, sort, direction)

列出用户 star 了的项目

列出用户 star 了的项目

### Example
```java
// Import classes:
//import com.gitee.api.api.ActivityApi;

ActivityApi apiInstance =  new ApiClient().create(ActivityApi.class);
String username = "username_example"; // String | 用户名(username/login)
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量
String sort = "created"; // String | 根据项目创建时间(created)或最后推送时间(updated)进行排序，默认：创建时间
String direction = "desc"; // String | 按递增(asc)或递减(desc)排序，默认：递减
Observable<java.util.List<Project>> result = apiInstance.getV5UsersUsernameStarred(username, accessToken, page, perPage, sort, direction);
result.subscribe(new Observer<java.util.List<Project>>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(java.util.List<Project> response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**| 用户名(username/login) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量 | [optional] [default to 20]
 **sort** | **String**| 根据项目创建时间(created)或最后推送时间(updated)进行排序，默认：创建时间 | [optional] [default to created] [enum: created, last_push]
 **direction** | **String**| 按递增(asc)或递减(desc)排序，默认：递减 | [optional] [default to desc] [enum: asc, desc]

### Return type

[**java.util.List&lt;Project&gt;**](Project.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5UsersUsernameSubscriptions"></a>
# **getV5UsersUsernameSubscriptions**
> java.util.List&lt;Project&gt; getV5UsersUsernameSubscriptions(username, accessToken, page, perPage, sort, direction)

列出用户 watch 了的项目

列出用户 watch 了的项目

### Example
```java
// Import classes:
//import com.gitee.api.api.ActivityApi;

ActivityApi apiInstance =  new ApiClient().create(ActivityApi.class);
String username = "username_example"; // String | 用户名(username/login)
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量
String sort = "created"; // String | 根据项目创建时间(created)或最后推送时间(updated)进行排序，默认：创建时间
String direction = "desc"; // String | 按递增(asc)或递减(desc)排序，默认：递减
Observable<java.util.List<Project>> result = apiInstance.getV5UsersUsernameSubscriptions(username, accessToken, page, perPage, sort, direction);
result.subscribe(new Observer<java.util.List<Project>>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(java.util.List<Project> response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**| 用户名(username/login) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量 | [optional] [default to 20]
 **sort** | **String**| 根据项目创建时间(created)或最后推送时间(updated)进行排序，默认：创建时间 | [optional] [default to created] [enum: created, last_push]
 **direction** | **String**| 按递增(asc)或递减(desc)排序，默认：递减 | [optional] [default to desc] [enum: asc, desc]

### Return type

[**java.util.List&lt;Project&gt;**](Project.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="patchV5NotificationsMessagesId"></a>
# **patchV5NotificationsMessagesId**
> Void patchV5NotificationsMessagesId(id, accessToken)

标记一个私信为已读

标记一个私信为已读

### Example
```java
// Import classes:
//import com.gitee.api.api.ActivityApi;

ActivityApi apiInstance =  new ApiClient().create(ActivityApi.class);
Integer id = 56; // Integer | 私信的 ID
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.patchV5NotificationsMessagesId(id, accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| 私信的 ID |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="patchV5NotificationsThreadsId"></a>
# **patchV5NotificationsThreadsId**
> Void patchV5NotificationsThreadsId(id, accessToken)

标记一个通知为已读

标记一个通知为已读

### Example
```java
// Import classes:
//import com.gitee.api.api.ActivityApi;

ActivityApi apiInstance =  new ApiClient().create(ActivityApi.class);
Integer id = 56; // Integer | 通知的 ID
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.patchV5NotificationsThreadsId(id, accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| 通知的 ID |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="postV5NotificationsMessages"></a>
# **postV5NotificationsMessages**
> Void postV5NotificationsMessages(username, content, accessToken)

发送私信给指定用户

发送私信给指定用户

### Example
```java
// Import classes:
//import com.gitee.api.api.ActivityApi;

ActivityApi apiInstance =  new ApiClient().create(ActivityApi.class);
String username = "username_example"; // String | 用户名(username/login)
String content = "content_example"; // String | 私信内容
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.postV5NotificationsMessages(username, content, accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**| 用户名(username/login) |
 **content** | **String**| 私信内容 |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="putV5NotificationsMessages"></a>
# **putV5NotificationsMessages**
> Void putV5NotificationsMessages(accessToken)

标记所有私信为已读

标记所有私信为已读

### Example
```java
// Import classes:
//import com.gitee.api.api.ActivityApi;

ActivityApi apiInstance =  new ApiClient().create(ActivityApi.class);
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.putV5NotificationsMessages(accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="putV5NotificationsThreads"></a>
# **putV5NotificationsThreads**
> Void putV5NotificationsThreads(accessToken)

标记所有通知为已读

标记所有通知为已读

### Example
```java
// Import classes:
//import com.gitee.api.api.ActivityApi;

ActivityApi apiInstance =  new ApiClient().create(ActivityApi.class);
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.putV5NotificationsThreads(accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="putV5ReposOwnerRepoNotifications"></a>
# **putV5ReposOwnerRepoNotifications**
> Void putV5ReposOwnerRepoNotifications(owner, repo, accessToken)

标记一个项目里的通知为已读

标记一个项目里的通知为已读

### Example
```java
// Import classes:
//import com.gitee.api.api.ActivityApi;

ActivityApi apiInstance =  new ApiClient().create(ActivityApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.putV5ReposOwnerRepoNotifications(owner, repo, accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="putV5UserStarredOwnerRepo"></a>
# **putV5UserStarredOwnerRepo**
> Void putV5UserStarredOwnerRepo(owner, repo, accessToken)

star 一个项目

star 一个项目

### Example
```java
// Import classes:
//import com.gitee.api.api.ActivityApi;

ActivityApi apiInstance =  new ApiClient().create(ActivityApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.putV5UserStarredOwnerRepo(owner, repo, accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="putV5UserSubscriptionsOwnerRepo"></a>
# **putV5UserSubscriptionsOwnerRepo**
> Void putV5UserSubscriptionsOwnerRepo(owner, repo, accessToken)

watch 一个项目

watch 一个项目

### Example
```java
// Import classes:
//import com.gitee.api.api.ActivityApi;

ActivityApi apiInstance =  new ApiClient().create(ActivityApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.putV5UserSubscriptionsOwnerRepo(owner, repo, accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

