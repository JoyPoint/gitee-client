
# Commit

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sha** | **String** |  |  [optional]
**author** | **String** |  |  [optional]
**committer** | **String** |  |  [optional]
**message** | **String** |  |  [optional]
**tree** | **String** |  |  [optional]
**parents** | **String** |  |  [optional]



