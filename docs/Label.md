
# Label

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**name** | **String** |  |  [optional]
**color** | **String** |  |  [optional]
**repositoryId** | **Integer** |  |  [optional]
**url** | **String** |  |  [optional]



