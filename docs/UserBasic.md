
# UserBasic

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**login** | **String** |  |  [optional]
**id** | **Integer** |  |  [optional]
**avatarUrl** | **String** |  |  [optional]
**url** | **String** |  |  [optional]
**htmlUrl** | **String** |  |  [optional]
**followersUrl** | **String** |  |  [optional]
**followingUrl** | **String** |  |  [optional]
**gistsUrl** | **String** |  |  [optional]
**starredUrl** | **String** |  |  [optional]
**subscriptionsUrl** | **String** |  |  [optional]
**organizationsUrl** | **String** |  |  [optional]
**reposUrl** | **String** |  |  [optional]
**eventsUrl** | **String** |  |  [optional]
**receivedEventsUrl** | **String** |  |  [optional]
**type** | **String** |  |  [optional]
**siteAdmin** | **String** |  |  [optional]



